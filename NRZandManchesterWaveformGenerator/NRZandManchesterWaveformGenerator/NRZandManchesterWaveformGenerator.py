import binascii
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np
#Read Inout file
with open('NRZandManchester_input_small.txt', mode = 'rb') as file:
    data = file.read()
text = bin(int(binascii.hexlify(data), 16)) #16 is the base for hex

#NRZ waveform


file = open("NRZ.txt", mode = "w")
count = 0
prevVal = ""
for bits in text.replace('b', ''):
          
    if prevVal != bits and prevVal != "":
        count += 1
        file.write("|")

    if bits == '0':
       file.write("__")
    elif bits == '1':
       file.write("──")

    prevVal = bits

file.write("\n")
file.write("The number of transition is: " +  str(count))
#Manchester Waveform

file = open("Manchester.txt", mode = "w")
count = 0
prevVal = ""
for bits in text.replace('b', ''):
          
    if prevVal == '0' and bits == '1':
        file.write("__")
    elif prevVal == '1' and bits == '0':
        file.write("──")
    elif prevVal == '0' and bits == '0':
        file.write("|")
        count += 1
    elif prevVal == '1' and bits == '1':
        file.write("|")
        count += 1

    if bits == '0':
       file.write("──|__")
       count += 1
    elif bits == '1':
       file.write("__|──") 
       count += 1

    prevVal = bits

file.write("\n")
file.write("The number of transition is: " +  str(count))



